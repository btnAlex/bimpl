let tab = function() {
    let tabItem = document.querySelectorAll('tab_item');
    let tabName;

    tabItem.forEach(item=> {
        item.addEventListener('click', selectTabItem)
    });

    function selectTabItem() {
        tabItem.forEach(item=> {
            item.classList.remove('active')
        });

        this.classList.add('active');
    }
};

tab();

